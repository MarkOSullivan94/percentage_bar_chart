package com.gozisoft.percentagebarchart;

import android.content.res.Resources;
import android.graphics.Paint;
import android.support.annotation.ColorInt;
import android.text.TextPaint;
import android.util.TypedValue;
import android.view.Gravity;

/**
 * Created by rgozim on 05/01/2017.
 */
public class Entry implements Comparable<Entry> {
    interface Callback {
        void onRefresh();

        void onOrderChange();
    }

    final Paint paint = new Paint();
    final TextPaint textPaint = new TextPaint();

    int order;
    float percentage;

    String label;
    int gravity = Gravity.START;

    Callback mCallback;

    Entry(int order, float percentage, @ColorInt int colour, String text, int gravity,
          @ColorInt int textColour, float textSize) {
        this.order = order;
        this.percentage = percentage;
        this.label = text;
        this.gravity = gravity;

        paint.setColor(colour);
        paint.setStyle(Paint.Style.FILL);

        textPaint.setColor(textColour);
        textPaint.setTextSize(textSize);
        textPaint.setTextAlign(Paint.Align.LEFT);
    }

    public void setText(String text) {
        this.label = text;
        triggerRefresh();
    }

    public void setTextColour(int colour) {
        this.textPaint.setColor(colour);
        triggerRefresh();
    }

    public void setGravity(int gravity) {
        this.gravity = gravity;
        triggerRefresh();
    }

    void setOrder(int order) {
        this.order = order;
        triggerSort();
        triggerRefresh();
    }

    int getGravity() {
        return gravity;
    }

    void triggerRefresh() {
        if (mCallback != null) {
            mCallback.onRefresh();
        }
    }

    void setCallback(Callback callback) {
        mCallback = callback;
    }

    void triggerSort() {
        if (mCallback != null) {
            mCallback.onOrderChange();
        }
    }

    @Override
    public int compareTo(Entry o) {
        return order - o.order;
    }

    public static class Builder {
        private String label;
        private float percentage;
        private float textSize;

        private int gravity;
        private int colour;
        private int textColour;
        private int order = -1;

        public Builder order(int order) {
            this.order = order;
            return this;
        }

        public Builder percentage(float percentage) {
            this.percentage = percentage;
            return this;
        }

        public Builder colour(@ColorInt int colour) {
            this.colour = colour;
            return this;
        }

        public Builder gravity(int gravity) {
            this.gravity = gravity;
            return this;
        }

        public Builder text(String text) {
            this.label = text;
            return this;
        }

        /**
         * Set the default text size to the given value, interpreted as "scaled
         * pixel" units.  This size is adjusted based on the current density and
         * user font size preference.
         */
        public Builder textSize(float size) {
            Resources r = Resources.getSystem();
            this.textSize = TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_SP, size, r.getDisplayMetrics());
            return this;
        }

        public Builder textColour(@ColorInt int color) {
            this.textColour = color;
            return this;
        }

        public Builder textPaddingLeft(int padding) {
            return this;
        }

        public Builder textPaddingRight(int padding) {
            return this;
        }

        public Entry build() {
            return new Entry(order, percentage, colour, label, gravity, textColour, textSize);
        }
    }
}
