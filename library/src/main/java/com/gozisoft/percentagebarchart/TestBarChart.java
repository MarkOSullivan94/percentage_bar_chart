package com.gozisoft.percentagebarchart;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.View;

import java.util.Collection;

/**
 * Draws a horizontal bar chart with colored slices, each represented by
 * {@link Entry}.
 */
public class TestBarChart extends View {
    private final Paint mEmptyPaint = new Paint();

    private Collection<Entry> mEntries;

    private int mMinTickWidth = 1;

    public static class Entry implements Comparable<Entry> {
        public final int mOrder;
        public final float mPercentage;
        public final Paint mPaint;

        protected Entry(int order, float percentage, Paint paint) {
            this.mOrder = order;
            this.mPercentage = percentage;
            this.mPaint = paint;
        }

        @Override
        public int compareTo(Entry another) {
            return mOrder - another.mOrder;
        }
    }

    public TestBarChart(Context context) {
        this(context, null);
    }

    public TestBarChart(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TestBarChart(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        // TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.PercentageBarChart);
        // mMinTickWidth = a.getDimensionPixelSize(R.styleable.PercentageBarChart_minTickWidth, 1);
        // int emptyColor = a.getColor(R.styleable.PercentageBarChart_emptyColor, Color.BLACK);
        // a.recycle();

        // mEmptyPaint.setColor(emptyColor);
        mEmptyPaint.setStyle(Paint.Style.FILL);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        final int left = getPaddingLeft();
        final int right = getWidth() - getPaddingRight();
        final int top = getPaddingTop();
        final int bottom = getHeight() - getPaddingBottom();

        final int width = right - left;

        final boolean isLayoutRtl = (ViewCompat.getLayoutDirection(this) ==
                ViewCompat.LAYOUT_DIRECTION_RTL);
        if (isLayoutRtl) {
            float nextX = right;

            if (mEntries != null) {
                for (final Entry e : mEntries) {
                    final float entryWidth;
                    if (e.mPercentage == 0.0f) {
                        entryWidth = 0.0f;
                    } else {
                        entryWidth = Math.max(mMinTickWidth, width * e.mPercentage);
                    }

                    final float lastX = nextX - entryWidth;
                    if (lastX < left) {
                        canvas.drawRect(left, top, nextX, bottom, e.mPaint);
                        return;
                    }

                    canvas.drawRect(lastX, top, nextX, bottom, e.mPaint);
                    nextX = lastX;
                }
            }

            canvas.drawRect(left, top, nextX, bottom, mEmptyPaint);
        } else {
            float lastX = left;

            if (mEntries != null) {
                for (final Entry e : mEntries) {
                    final float entryWidth;
                    if (e.mPercentage == 0.0f) {
                        entryWidth = 0.0f;
                    } else {
                        entryWidth = Math.max(mMinTickWidth, width * e.mPercentage);
                    }

                    final float nextX = lastX + entryWidth;
                    if (nextX > right) {
                        canvas.drawRect(lastX, top, right, bottom, e.mPaint);
                        return;
                    }

                    canvas.drawRect(lastX, top, nextX, bottom, e.mPaint);
                    lastX = nextX;
                }
            }

            canvas.drawRect(lastX, top, right, bottom, mEmptyPaint);
        }
    }

    /**
     * Sets the background for this chart. Callers are responsible for later
     * calling {@link #invalidate()}.
     */
    @Override
    public void setBackgroundColor(int color) {
        mEmptyPaint.setColor(color);
    }

    /**
     * Adds a new slice to the percentage bar chart. Callers are responsible for
     * later calling {@link #invalidate()}.
     *
     * @param percentage the total width that
     * @param color      the color to draw the entry
     */
    public static Entry createEntry(int order, float percentage, int color) {
        final Paint p = new Paint();
        p.setColor(color);
        p.setStyle(Paint.Style.FILL);
        return new Entry(order, percentage, p);
    }

    public void setEntries(Collection<Entry> entries) {
        mEntries = entries;
    }

    public Collection<Entry> getEntries() {
        return mEntries;
    }
}