package com.gozisoft.percentagebarchart;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Build;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;

import java.util.ArrayList;
import java.util.Collections;

import static com.gozisoft.percentagebarchart.DrawUtils.RoundedRect;

/**
 * Created by rgozim on 30/12/2016.
 */

public class PercentageBarChart extends View {

    public static final String TAG = PercentageBarChart.class.getSimpleName();

    private Entry.Callback mCallback = new Entry.Callback() {
        @Override
        public void onRefresh() {
            invalidate();
            requestLayout();
        }

        @Override
        public void onOrderChange() {
            Collections.sort(mEntries);
            invalidate();
            requestLayout();
        }
    };

    private final Paint mEmptyPaint = new Paint();
    private final Rect mTextBounds = new Rect();
    private final Rect mDrawBounds = new Rect();

    private ArrayList<Entry> mEntries;
    private Direction mDirection;

    private int mMinTickWidth;

    private int mRx;
    private int mRy;

    public PercentageBarChart(Context context) {
        this(context, null);
    }

    public PercentageBarChart(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PercentageBarChart(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr, 0);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public PercentageBarChart(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        mEntries = new ArrayList<>();

        TypedArray ta = null;
        try {
            ta = context.getTheme().obtainStyledAttributes(attrs, R.styleable.PercentageBarChart,
                    defStyleAttr, defStyleRes);

            mDirection = Direction.fromId(ta.getInt(R.styleable.PercentageBarChart_direction,
                    Direction.left_to_right.id));

            mMinTickWidth = ta.getDimensionPixelSize(R.styleable.PercentageBarChart_minTickWidth, 1);

            int emptyColor = ta.getColor(R.styleable.PercentageBarChart_emptyColor,
                    Color.TRANSPARENT);
            mEmptyPaint.setColor(emptyColor);
            mEmptyPaint.setStyle(Paint.Style.FILL);

            int radius = ta.getDimensionPixelSize(R.styleable.PercentageBarChart_radius, -1);
            if (radius == -1) {
                mRx = ta.getInteger(R.styleable.PercentageBarChart_rx, 0);
                mRy = ta.getInteger(R.styleable.PercentageBarChart_ry, 0);
            } else {
                mRx = mRy = radius;
            }
        } finally {
            if (ta != null) {
                ta.recycle();
            }
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // Draw background first
        canvas.drawRect(mDrawBounds.left, mDrawBounds.top, mDrawBounds.right,
                mDrawBounds.bottom, mEmptyPaint);

        final int totalWidth = mDrawBounds.width();

        // Total entry values
        float maxEntryValue = 0;
        float minEntryValue = 0;
        for (Entry entry : mEntries) {
            maxEntryValue = Math.max(maxEntryValue, entry.percentage);
            minEntryValue = Math.min(minEntryValue, entry.percentage);
        }

        float valueDelta = maxEntryValue - minEntryValue;

        switch (mDirection) {
            case left_to_right:
                float lastX = mDrawBounds.left;
                for (Entry entry : mEntries) {
                    float scaled = (entry.percentage - minEntryValue) / valueDelta;

                    // Calculate width for given mPercentage
                    float entryWidth = Math.max(mMinTickWidth, totalWidth * scaled);

                    float right = lastX + entryWidth;
                    canvas.drawPath(RoundedRect(lastX, mDrawBounds.top, right, mDrawBounds.bottom, mRx, mRy,
                            false, true, true, false), entry.paint);
                    lastX = right;
                }
                break;

            case right_to_left:
                float right = mDrawBounds.right;
                for (Entry entry : mEntries) {
                    float entryWidth = Math.max(mMinTickWidth, totalWidth * entry.percentage);

                    float left = right - entryWidth;
                    canvas.drawPath(RoundedRect(left, mDrawBounds.top, right, mDrawBounds.bottom, mRx, mRy,
                            true, false, false, true), entry.paint);
                    right = left;

                    // Now draw the text
                    drawText(canvas, entry, entryWidth);
                }
                break;
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        // Skip if empty
        if (mEntries.isEmpty()) {
            return;
        }

        // Set the bounds for drawing
        mDrawBounds.set(getPaddingLeft(), getPaddingTop(),
                w - getPaddingLeft(), h - getPaddingTop());

        // int height = h - (getPaddingTop() + getPaddingBottom());
        // int width = w - (getPaddingLeft() + getPaddingRight());
        int actualHeight = mDrawBounds.height() - mRy;
        int actualWidth = mDrawBounds.width() - mRx;

        // Constrain each text height to less than that of the bar chart
        for (Entry entry : mEntries) {
            entry.textPaint.getTextBounds(entry.label, 0, entry.label.length(), mTextBounds);

            // If the text is a greater height than the view, then
            // loop until we can find a size that fits.
            while (true) {
                // final int textHeight = (int) getTextHeight(entry.textPaint);
                if (mTextBounds.height() < actualHeight) {
                    break;
                }

                // Shrink the height of the text until we can ge a height that
                // fits.
                entry.textPaint.setTextSize(entry.textPaint.getTextSize() * 0.9f);
            }
        }
    }

    public void add(Entry entry) {
        // If the order is -1, then we have to assign one automatically
        if (entry.order == -1) {
            entry.order = mEntries.size();
        }

        // Add entry to the collection
        mEntries.add(entry);

        // Entry communicates back to the bar chart via a callback
        entry.setCallback(mCallback);

        // Sort the entries and trigger a redraw
        Collections.sort(mEntries);
        invalidate();
        requestLayout();
    }

    private void drawText(Canvas canvas, Entry entry, float entryWidth) {
        // Now draw the text
        final float curveWidth = mRx; // cornerLength(mRx);
        final float widthMinusCurve = entryWidth - curveWidth;

        // If the width of the text is less than the width of the bar
        final float textWidth = entry.textPaint.measureText(entry.label);
        if (textWidth > widthMinusCurve) {
            return;
        }

        entry.textPaint.getTextBounds(entry.label, 0, entry.label.length(), mTextBounds);
        int textHeight = mTextBounds.height(); // getTextHeight(entry.mTextPaint);

        // Calculate center y of view and offset vertically down using
        // half text height to render in exact center
        float y = (getHeight() >> 1) + (textHeight >> 1);
        float x;

        // Calculate where to lie on the horizontal
        final int gravity = Gravity.getAbsoluteGravity(entry.gravity, getLayoutDirection());
        switch (gravity) {
            case Gravity.START:
                x = mTextBounds.left;
                break;
            case Gravity.END:
                float drawRight = mTextBounds.left - mDrawBounds.right;
                x = drawRight - textWidth;
                break;
            default:
                throw new RuntimeException("Unsupported gravity");
        }
        canvas.drawText(entry.label, x, y, entry.textPaint);
    }



}
