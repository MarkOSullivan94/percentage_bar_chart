package com.gozisoft.percentagebarchart;

/**
 * Created by rgozim on 05/01/2017.
 */
public enum Direction {
    left_to_right(0),
    right_to_left(1);

    int id;

    Direction(int id) {
        this.id = id;
    }

    public static Direction fromId(int id) {
        for (Direction d : values()) {
            if (d.id == id) return d;
        }
        throw new IllegalArgumentException();
    }
}
