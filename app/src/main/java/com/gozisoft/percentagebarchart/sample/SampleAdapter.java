package com.gozisoft.percentagebarchart.sample;

import android.animation.ObjectAnimator;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gozisoft.percentagebarchart.HorizontalBar;
import com.gozisoft.percentagebarchart.PercentageBarChartLayout;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by rgozim on 22/01/2017.
 */
public class SampleAdapter extends RecyclerView.Adapter<SampleAdapter.ViewHolder> {

    private List<String> mLabels;
    private List<Float> mData;
    private Set<Integer> mAnimated;

    public SampleAdapter(String[] labels, float[] values) {
        mLabels = Arrays.asList(labels);
        mData = normaliseData(values);
        mAnimated = new HashSet<>(values.length);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewGroup root = (ViewGroup) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_row_bar_chart, parent, false);

        PercentageBarChartLayout barChartLayout = (PercentageBarChartLayout)
                root.findViewById(R.id.barChart);

        int textColor = ContextCompat.getColor(parent.getContext(), android.R.color.white);

        barChartLayout.addEntry(new HorizontalBar.Entry()
                .setDrawable(R.drawable.bar_chart_rounded_rect)
                .setTextSize(18.0f)
                .setTextColor(textColor));

        return new ViewHolder(root);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // Set the label of the current list item
        String label = mLabels.get(position);
        if (!TextUtils.isEmpty(label) && holder.mLabel != null) {
            holder.mLabel.setText(label);
        }

        // Set the percentage for bar of the current list item
        float value = mData.get(position);
        if (!mAnimated.contains(position)) {
            mAnimated.add(position);

            // This view hasn't been displayed before, animate the setting of percentage
            ObjectAnimator anim = ObjectAnimator.ofFloat(holder.mItem, "percentage", 0, value);
            anim.setDuration(1000);
            anim.start();
        } else {
            holder.mItem.setPercentage(value);
        }
        holder.mItem.setText(String.valueOf(value));
        holder.mItem.setTextColorRes(R.color.md_grey_800);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    /**
     * Normalise a data set of values between
     * zero and one.
     *
     * @param input
     * @return
     */
    static List<Float> normaliseData(float[] input) {
        float min = 0.0f;
        float max = 0.0f;
        for (float value : input) {
            min = Math.min(min, value);
            max = Math.max(max, value);
        }

        List<Float> output = new ArrayList<>(input.length);

        // Equation used is:
        // d = input value
        // n = range value
        // v = [(x - min(d)) * (max(n) - min(n)) / max(d) - min(d)] + min(n)
        for (float value : input) {
            float percentage = (value - min) / max - min;
            output.add(percentage);
        }

        return output;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView mLabel;
        PercentageBarChartLayout mBarChart;
        HorizontalBar mItem;

        public ViewHolder(View itemView) {
            super(itemView);
            mLabel = (TextView) itemView.findViewById(R.id.label);
            mBarChart = (PercentageBarChartLayout)
                    itemView.findViewById(R.id.barChart);
            mItem = (HorizontalBar) mBarChart.getChildAt(0);
        }
    }

}
