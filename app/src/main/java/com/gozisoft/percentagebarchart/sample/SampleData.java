package com.gozisoft.percentagebarchart.sample;

/**
 * Created by Riad on 23/01/2017.
 */

public class SampleData {

    public static final float[] VALUES = new float[]{
            30.0f,
            2.0f,
            200.0f,
            50.0f,
            122.0f,
            400.0f,
            100.0f,
            12.0f,
            40.0f,
            140.0f,
            180.0f,
            30.0f,
            2.0f,
            200.0f,
            50.0f,
            122.0f,
            400.0f,
            100.0f,
            12.0f,
            40.0f,
            140.0f,
    };

    public static final String[] LABELS = new String[]{
            "vulputate",
            "nisi",
            "a",
            "vulputate",
            "amet",
            "neque",
            "dolor,",
            "lectus",
            "semper",
            "lorem,",
            "Cras",
            "metus.",
            "et",
            "malesuada",
            "vitae,",
            "pharetra",
            "Aenean",
            "dui,",
            "magnis",
            "ligula",
            "posuere",
    };

}
